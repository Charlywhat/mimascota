/** @type {import('./$types').PageLoad} */
export async function load({ params }) {
    // Get pet info
    const url = "https://us-central1-techme-335017.cloudfunctions.net/mimascota-get"
    let res = await fetch(`${url}?p=${params.id}`)
    console.log(res)
    if (res.ok) {
        let pet_info = await res.json()
        return {
            data: pet_info
        }
    }
    return {}
}