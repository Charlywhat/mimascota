## Mi Mascota

It's a website that hosts and shows pets basic info:
 - Name
 - Sex
 - Age
 - Owner's Address
 - Owner's Phone Number

## Backend

The Backend logic is written in Golang, it is hosted in Google Cloud Platform and it uses Mongo DB to store pets information.

## TODOs
- [ ] Add pets picture URL
- [ ] Add FE logic
- [ ] Test FE and BE integration locally
- [ ] Host website

