// Package p contains an HTTP Cloud Function.
package p

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"os"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type ContactInfo struct {
	Address string `json:"address" bson:"address"`
	Phone   string `json:"phone" bson:"phone"`
}

type Pet struct {
	Name        string      `json:"name" bson:"name"`
	Sex         string      `json:"sex" bson:"sex"`
	Age         int         `json:"age" bson:"age"`
	Contact     ContactInfo `json:"contact" bson:"contact"`
	Description string      `json:"description" bson:"description"`
	Picture     string      `json:"picture" bson:"picture"`
}

type Secret struct {
	ConnectionString string `json:"conn_string"`
	DataBase         string `json:"db_name"`
	Collection       string `json:"collection_name"`
}

func GetInfo(w http.ResponseWriter, r *http.Request) {
	// Handle HTTP GET only
	if r.Method != http.MethodGet {
		http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
		return
	}
	secret, err := getSecrets()
	if err != nil {
		log.Println("Could not get secrets:")
		log.Panicln(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	petId := r.URL.Query().Get("p")
	if petId == "" {
		log.Println("Missing pet ID")
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	pets, err := getPet(secret, petId)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	res, err := json.Marshal(pets)
	if err != nil {
		log.Fatal(err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	w.Write(res)
}

func getSecrets() (Secret, error) {
	secret := new(Secret)
	// Get GCloud Function relative dir
	gcloudFuncSourceDir := "serverless_function_source_code"
	fileInfo, err := os.Stat(gcloudFuncSourceDir)
	if err == nil && fileInfo.IsDir() {
		_ = os.Chdir(gcloudFuncSourceDir)
	}
	//
	fname := "secrets.json"
	content, err := os.ReadFile(fname)
	if err != nil {
		return *secret, err
	}
	err = json.Unmarshal(content, secret)
	return *secret, nil
}

func getPet(secret Secret, petId string) (Pet, error) {
	var result Pet
	// Use the SetServerAPIOptions() method to set the Stable API version to 1
	serverAPIOptions := options.ServerAPI(options.ServerAPIVersion1)

	clientOptions := options.Client().
		ApplyURI(secret.ConnectionString).
		SetServerAPIOptions(serverAPIOptions)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.Fatal(err)
		panic(err)
	}
	// Get all docs from DB collection
	collection := client.Database(secret.DataBase).Collection(secret.Collection)
	err = collection.FindOne(context.Background(), bson.D{{"_id", petId}}).Decode(&result)
	if err != nil {
		log.Fatal(err)
		return result, err
	}
	return result, nil
}
